1/ Cài đặt Go với nhiều version:
- Cài đặt go:
  + $ git clone https://go.googlesource.com/go goroot
  + $ cd goroot
  + $ git checkout master
  + $ cd src
  + $ ./all.bash
- Để cài đặt thêm go version khác sử dụng command: "go install".
  ví dụ: để cài go1.12.10
  + $ go install golang.org/dl/go1.12.10@latest
  + $ go1.12.10 download
  + kiểm tra nơi cài đặt của phiên bản: $ go1.12.10 env GOROOT
- Tham khảo các version Go: https://go.dev/dl/
