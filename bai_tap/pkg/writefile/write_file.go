package writefile

import (
	"bufio"
	"os"
	"strconv"
)

func WriteLines(lines []int, path string) error {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()

	var str string
	w := bufio.NewWriter(file)
	for _, line := range lines {
		str = strconv.Itoa(line)
		w.WriteString(str + " ")
	}

	return w.Flush()
}

func Writestr(str string, path string) error {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()
	w := bufio.NewWriter(file)
	w.WriteString(str)
	return w.Flush()
}

func Writeint(i int, path string) error {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()
	w := bufio.NewWriter(file)
	var str1 string
	str1 = strconv.Itoa(i)
	w.WriteString(str1)
	return w.Flush()
}
