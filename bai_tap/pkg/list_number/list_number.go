package list_number

import (
	"bai_tap/pkg/integer"
)

func List_even_number(a []int) []int {
	var result []int
	for i := 0; i < len(a); i++ {
		if a[i]%2 == 0 {
			result = append(result, a[i])
		}
	}
	return result
}

func List_even_number_4(a []int) []int {
	var result []int
	for i := 1; i < len(a); i++ {
		if a[i]%2 == 0 {
			result = append(result, a[i])
		}
	}
	return result
}

func Sum_odd(a []int) int {
	result := 0
	for i := 0; i < len(a); i++ {
		if a[i]%2 != 0 {
			result += a[i]
		}
	}
	return result
}

func Sum_odd_4(a []int) int {
	result := 0
	for i := 1; i < len(a); i++ {
		if a[i]%2 != 0 {
			result += a[i]
		}
	}
	return result
}

func Search_b(a []int, b int) int {
	count := 0
	for i := 0; i < len(a); i++ {
		if a[i] == b {
			count++
		}
	}
	return count
}

func Search_b_4(a []int, b int) int {
	count := 0
	for i := 1; i < len(a); i++ {
		if a[i] == b {
			count++
		}
	}
	return count
}

func Search_Songuyento(a []int) []int {
	var n []int
	for i := 0; i < len(a); i++ {
		if integer.Search_Songuyento(a[i]) == true {
			n = append(n, a[i])
		}
	}
	return n
}

func Search_Songuyento_4(a []int) []int {
	var n []int
	for i := 1; i < len(a); i++ {
		if integer.Search_Songuyento(a[i]) == true {
			n = append(n, a[i])
		}
	}
	return n
}
