package integer

func Search_SochinhPhuong(n int) int {
	for i := 1; i*i <= n; i++ {
		if i*i == n {
			return 1
		}
	}
	return 0
}

func Search_Songuyento(n int) bool {
	b := n / 2
	if n < 2 {
		return false
	}
	for i := 2; i <= b; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func Search_Namnhuan(year int) int {
	if year%400 == 0 {
		return 0
	}
	if year%4 == 0 && year%100 != 0 {
		return 0
	}
	return 1
}

func Search_MultiNamnhuan(Startyear, EndYear int) []int {
	var a []int
	for i := Startyear; i <= EndYear; i++ {
		if i%400 == 0 {
			a = append(a, i)
		}
		if i%4 == 0 && i%100 != 0 {
			a = append(a, i)
		}
	}
	return a
}
