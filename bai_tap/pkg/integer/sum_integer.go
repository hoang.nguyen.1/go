package integer

func Sumint1(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		result += i
	}
	return result
}

func Sumint2(n int) int {
	result := 0
	for i := 0; i <= n; i++ {
		result += (2*i + 1)
	}
	return result
}

func Sumint3(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		q := 1
		for j := 1; j <= i; j++ {
			q *= i
		}
		result += q
	}
	return result
}

func Sumint4(n int) float64 {
	result := 0.0
	for i := 1; i <= n; i++ {
		result += 1.0 / float64(i)
	}
	return result
}

func Sumint5(n int) int {
	result := 1
	for i := 1; i <= n; i++ {
		result *= 2 * i
	}
	return result
}

func Fibo(f int) int {
	Fib := []int{1, 1}
	for i := 3; i <= f; i++ {
		nextFib := Fib[0] + Fib[1]
		Fib = []int{Fib[1], nextFib}
	}
	if f >= 1 {
		return Fib[1]
	}
	return Fib[0]
}

func SumFibo(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		F := Fibo(i)
		result += F
	}
	return result
}
