| Project name | Coverrage |
| -- | -- |
| [thien.ly / services-transcode](https://gitlab.vieon.vn/thien.ly/services-transcode) | ![No badge](https://gitlab.vieon.vn/thien.ly/services-transcode/badges/master/coverage.svg) |
| [data / api-lookback](https://gitlab.vieon.vn/data/api-lookback) | ![No badge](https://gitlab.vieon.vn/data/api-lookback/badges/master/coverage.svg) |
| [git-operation-cloud / token](https://gitlab.vieon.vn/git-operation-cloud/token) | ![No badge](https://gitlab.vieon.vn/git-operation-cloud/token/badges/master/coverage.svg) |
| [vieservices / payment-services-docs](https://gitlab.vieon.vn/vieservices/payment-services-docs) | ![No badge](https://gitlab.vieon.vn/vieservices/payment-services-docs/badges/master/coverage.svg) |
| [vieservices / user-services-docs](https://gitlab.vieon.vn/vieservices/user-services-docs) | ![No badge](https://gitlab.vieon.vn/vieservices/user-services-docs/badges/master/coverage.svg) |
| [data / vieon_watch_time_filter](https://gitlab.vieon.vn/data/vieon_watch_time_filter) | ![No badge](https://gitlab.vieon.vn/data/vieon_watch_time_filter/badges/master/coverage.svg) |
| [Quality Assurance / API Automation v2](https://gitlab.vieon.vn/quality-assurance/api-automation-v2) | ![No badge](https://gitlab.vieon.vn/quality-assurance/api-automation-v2/badges/master/coverage.svg) |
| [data / api-sync-cache-recommend](https://gitlab.vieon.vn/data/api-sync-cache-recommend) | ![No badge](https://gitlab.vieon.vn/data/api-sync-cache-recommend/badges/master/coverage.svg) |
| [Viezone / Search API](https://gitlab.vieon.vn/viezone/search-api) | ![No badge](https://gitlab.vieon.vn/viezone/search-api/badges/master/coverage.svg) |
| [vieon-services / telco-vnpt-sms](https://gitlab.vieon.vn/vieon-services/telco-vnpt-sms) | ![No badge](https://gitlab.vieon.vn/vieon-services/telco-vnpt-sms/badges/master/coverage.svg) |
| [vieon-services / marketing-campaign](https://gitlab.vieon.vn/vieon-services/marketing-campaign) | ![No badge](https://gitlab.vieon.vn/vieon-services/marketing-campaign/badges/master/coverage.svg) |
| [Artificial Intelligent / Age-And-Gender](https://gitlab.vieon.vn/artificial-intelligent/age-and-gender) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/age-and-gender/badges/master/coverage.svg) |
| [vieon-services / redis](https://gitlab.vieon.vn/vieon-services/redis) | ![No badge](https://gitlab.vieon.vn/vieon-services/redis/badges/master/coverage.svg) |
| [git-operation-cloud / test1q11](https://gitlab.vieon.vn/git-operation-cloud/test1q11) | ![No badge](https://gitlab.vieon.vn/git-operation-cloud/test1q11/badges/master/coverage.svg) |
| [data / vieon-visited-user](https://gitlab.vieon.vn/data/vieon-visited-user) | ![No badge](https://gitlab.vieon.vn/data/vieon-visited-user/badges/master/coverage.svg) |
| [platform / sre / docker files](https://gitlab.vieon.vn/platform/sre/docker-files) | ![No badge](https://gitlab.vieon.vn/platform/sre/docker-files/badges/master/coverage.svg) |
| [platform / workloads / database-production](https://gitlab.vieon.vn/platform/workloads/database-production) | ![No badge](https://gitlab.vieon.vn/platform/workloads/database-production/badges/master/coverage.svg) |
| [platform / workloads / database-non-production](https://gitlab.vieon.vn/platform/workloads/database-non-production) | ![No badge](https://gitlab.vieon.vn/platform/workloads/database-non-production/badges/master/coverage.svg) |
| [platform / sre / vie-operator](https://gitlab.vieon.vn/platform/sre/vie-operator) | ![No badge](https://gitlab.vieon.vn/platform/sre/vie-operator/badges/master/coverage.svg) |
| [Quality Assurance / VieON_web_auto_v2](https://gitlab.vieon.vn/quality-assurance/vieon_web_auto_v2) | ![No badge](https://gitlab.vieon.vn/quality-assurance/vieon_web_auto_v2/badges/master/coverage.svg) |
| [lab-k8s / k8s-lab-cicd](https://gitlab.vieon.vn/lab-k8s/k8s-lab-cicd) | ![No badge](https://gitlab.vieon.vn/lab-k8s/k8s-lab-cicd/badges/master/coverage.svg) |
| [Trương Quốc Thắng / lab-k8s](https://gitlab.vieon.vn/thang.truong/lab-k8s) | ![No badge](https://gitlab.vieon.vn/thang.truong/lab-k8s/badges/master/coverage.svg) |
| [data / Flink Impl](https://gitlab.vieon.vn/data/flink-impl) | ![No badge](https://gitlab.vieon.vn/data/flink-impl/badges/master/coverage.svg) |
| [Quality Assurance / Auto Mobile Web](https://gitlab.vieon.vn/quality-assurance/auto-mobile-web) | ![No badge](https://gitlab.vieon.vn/quality-assurance/auto-mobile-web/badges/master/coverage.svg) |
| [data / sdk-webos-normalizing](https://gitlab.vieon.vn/data/sdk-webos-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-webos-normalizing/badges/master/coverage.svg) |
| [data / sdk-web-normalizing](https://gitlab.vieon.vn/data/sdk-web-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-web-normalizing/badges/master/coverage.svg) |
| [data / sdk-tizen-normalizing](https://gitlab.vieon.vn/data/sdk-tizen-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-tizen-normalizing/badges/master/coverage.svg) |
| [data / sdk-android-tv-normalizing](https://gitlab.vieon.vn/data/sdk-android-tv-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-android-tv-normalizing/badges/master/coverage.svg) |
| [data / sdk-ios-normalizing](https://gitlab.vieon.vn/data/sdk-ios-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-ios-normalizing/badges/master/coverage.svg) |
| [data / sdk-android-normalizing](https://gitlab.vieon.vn/data/sdk-android-normalizing) | ![No badge](https://gitlab.vieon.vn/data/sdk-android-normalizing/badges/master/coverage.svg) |
| [Đào Lê Trinh  / game-voting](https://gitlab.vieon.vn/trinh.dao/game-voting) | ![No badge](https://gitlab.vieon.vn/trinh.dao/game-voting/badges/master/coverage.svg) |
| [platform / sre / workloads-sre / production](https://gitlab.vieon.vn/platform/sre/workloads-sre/production) | ![No badge](https://gitlab.vieon.vn/platform/sre/workloads-sre/production/badges/master/coverage.svg) |
| [platform / sre / workloads-sre / testing](https://gitlab.vieon.vn/platform/sre/workloads-sre/testing) | ![No badge](https://gitlab.vieon.vn/platform/sre/workloads-sre/testing/badges/master/coverage.svg) |
| [platform / sre / workloads-sre / develop](https://gitlab.vieon.vn/platform/sre/workloads-sre/develop) | ![No badge](https://gitlab.vieon.vn/platform/sre/workloads-sre/develop/badges/master/coverage.svg) |
| [git-operation-cloud / eks-scale-job](https://gitlab.vieon.vn/git-operation-cloud/eks-scale-job) | ![No badge](https://gitlab.vieon.vn/git-operation-cloud/eks-scale-job/badges/master/coverage.svg) |
| [git-operation-cloud / teraform](https://gitlab.vieon.vn/git-operation-cloud/teraform) | ![No badge](https://gitlab.vieon.vn/git-operation-cloud/teraform/badges/master/coverage.svg) |
| [data / vieon_request_log_transform](https://gitlab.vieon.vn/data/vieon_request_log_transform) | ![No badge](https://gitlab.vieon.vn/data/vieon_request_log_transform/badges/master/coverage.svg) |
| [platform / workloads / production](https://gitlab.vieon.vn/platform/workloads/production) | ![No badge](https://gitlab.vieon.vn/platform/workloads/production/badges/master/coverage.svg) |
| [platform / workloads / testing](https://gitlab.vieon.vn/platform/workloads/testing) | ![No badge](https://gitlab.vieon.vn/platform/workloads/testing/badges/master/coverage.svg) |
| [platform / sre / toy-cicd](https://gitlab.vieon.vn/platform/sre/toy-cicd) | ![No badge](https://gitlab.vieon.vn/platform/sre/toy-cicd/badges/master/coverage.svg) |
| [Nguyen Thanh Giang / test](https://gitlab.vieon.vn/giang.nguyen.thanh/test) | ![No badge](https://gitlab.vieon.vn/giang.nguyen.thanh/test/badges/master/coverage.svg) |
| [Artificial Intelligent / video-color-analysis](https://gitlab.vieon.vn/artificial-intelligent/video-color-analysis) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/video-color-analysis/badges/master/coverage.svg) |
| [platform / workloads / develop](https://gitlab.vieon.vn/platform/workloads/develop) | ![No badge](https://gitlab.vieon.vn/platform/workloads/develop/badges/master/coverage.svg) |
| [platform / sre / Helm Charts](https://gitlab.vieon.vn/platform/sre/helm-charts) | ![No badge](https://gitlab.vieon.vn/platform/sre/helm-charts/badges/master/coverage.svg) |
| [vie-labs / Go API](https://gitlab.vieon.vn/vie-labs/go-api) | ![No badge](https://gitlab.vieon.vn/vie-labs/go-api/badges/master/coverage.svg) |
| [vie-labs / python-crash-course](https://gitlab.vieon.vn/vie-labs/python-crash-course) | ![No badge](https://gitlab.vieon.vn/vie-labs/python-crash-course/badges/master/coverage.svg) |
| [vie-labs / gitlab-features](https://gitlab.vieon.vn/vie-labs/gitlab-features) | ![No badge](https://gitlab.vieon.vn/vie-labs/gitlab-features/badges/master/coverage.svg) |
| [data / api-data-content-moengate](https://gitlab.vieon.vn/data/api-data-content-moengate) | ![No badge](https://gitlab.vieon.vn/data/api-data-content-moengate/badges/master/coverage.svg) |
| [platform / docker-images](https://gitlab.vieon.vn/platform/docker-images) | ![No badge](https://gitlab.vieon.vn/platform/docker-images/badges/master/coverage.svg) |
| [secret-keys / vieon-ios-keys](https://gitlab.vieon.vn/secret-keys/vieon-ios-keys) | ![No badge](https://gitlab.vieon.vn/secret-keys/vieon-ios-keys/badges/master/coverage.svg) |
| [Nguyễn Hoàng Phúc / Index Search](https://gitlab.vieon.vn/phuc.nguyen/index-search) | ![No badge](https://gitlab.vieon.vn/phuc.nguyen/index-search/badges/master/coverage.svg) |
| [thang-test-project / gatling](https://gitlab.vieon.vn/thang-test-project/gatling) | ![No badge](https://gitlab.vieon.vn/thang-test-project/gatling/badges/master/coverage.svg) |
| [platform / gitops-retrancode](https://gitlab.vieon.vn/platform/gitops-retrancode) | ![No badge](https://gitlab.vieon.vn/platform/gitops-retrancode/badges/master/coverage.svg) |
| [thang-test-project / vault](https://gitlab.vieon.vn/thang-test-project/vault) | ![No badge](https://gitlab.vieon.vn/thang-test-project/vault/badges/master/coverage.svg) |
| [thang-test-project / pl-loadtest](https://gitlab.vieon.vn/thang-test-project/pl-loadtest) | ![No badge](https://gitlab.vieon.vn/thang-test-project/pl-loadtest/badges/master/coverage.svg) |
| [Artificial Intelligent / ai-api-wrapper](https://gitlab.vieon.vn/artificial-intelligent/ai-api-wrapper) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/ai-api-wrapper/badges/master/coverage.svg) |
| [vieon-services / transcoder-worker](https://gitlab.vieon.vn/vieon-services/transcoder-worker) | ![No badge](https://gitlab.vieon.vn/vieon-services/transcoder-worker/badges/master/coverage.svg) |
| [vieon-services / cross-checking-service](https://gitlab.vieon.vn/vieon-services/cross-checking-service) | ![No badge](https://gitlab.vieon.vn/vieon-services/cross-checking-service/badges/master/coverage.svg) |
| [vieon-services / Live Comment Consumer](https://gitlab.vieon.vn/vieon-services/live-comment-consumer) | ![No badge](https://gitlab.vieon.vn/vieon-services/live-comment-consumer/badges/master/coverage.svg) |
| [minhduy / test](https://gitlab.vieon.vn/minhduy/test) | ![No badge](https://gitlab.vieon.vn/minhduy/test/badges/master/coverage.svg) |
| [Artificial Intelligent / task-pipeline-dashboard](https://gitlab.vieon.vn/artificial-intelligent/task-pipeline-dashboard) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/task-pipeline-dashboard/badges/master/coverage.svg) |
| [data / trigger_hot_content](https://gitlab.vieon.vn/data/trigger_hot_content) | ![No badge](https://gitlab.vieon.vn/data/trigger_hot_content/badges/master/coverage.svg) |
| [platform / k8s-setup](https://gitlab.vieon.vn/platform/k8s-setup) | ![No badge](https://gitlab.vieon.vn/platform/k8s-setup/badges/master/coverage.svg) |
| [platform / k8s-presetup](https://gitlab.vieon.vn/platform/k8s-presetup) | ![No badge](https://gitlab.vieon.vn/platform/k8s-presetup/badges/master/coverage.svg) |
| [vieon-services / telco-mobifone](https://gitlab.vieon.vn/vieon-services/telco-mobifone) | ![No badge](https://gitlab.vieon.vn/vieon-services/telco-mobifone/badges/master/coverage.svg) |
| [vieon-services / cdn-router](https://gitlab.vieon.vn/vieon-services/cdn-routing) | ![No badge](https://gitlab.vieon.vn/vieon-services/cdn-routing/badges/master/coverage.svg) |
| [test-duy / clear-cache](https://gitlab.vieon.vn/test-duy/clear-cache) | ![No badge](https://gitlab.vieon.vn/test-duy/clear-cache/badges/master/coverage.svg) |
| [Quality Assurance / auto-android-tv](https://gitlab.vieon.vn/quality-assurance/auto-android-tv) | ![No badge](https://gitlab.vieon.vn/quality-assurance/auto-android-tv/badges/master/coverage.svg) |
| [vieon-services / billing-consumer](https://gitlab.vieon.vn/vieon-services/billing-consumer) | ![No badge](https://gitlab.vieon.vn/vieon-services/billing-consumer/badges/master/coverage.svg) |
| [Quality Assurance / ott-services / test-trigger-pipeline](https://gitlab.vieon.vn/quality-assurance/ott-services/test-trigger-pipeline) | ![No badge](https://gitlab.vieon.vn/quality-assurance/ott-services/test-trigger-pipeline/badges/master/coverage.svg) |
| [Võ Khanh Toàn / S3](https://gitlab.vieon.vn/toan.vo/s3) | ![No badge](https://gitlab.vieon.vn/toan.vo/s3/badges/master/coverage.svg) |
| [vieon-services / bot-comment](https://gitlab.vieon.vn/vieon-services/bot-comment) | ![No badge](https://gitlab.vieon.vn/vieon-services/bot-comment/badges/master/coverage.svg) |
| [data / kafka_cluster](https://gitlab.vieon.vn/data/kafka_cluster) | ![No badge](https://gitlab.vieon.vn/data/kafka_cluster/badges/master/coverage.svg) |
| [data / Scylla Cluster](https://gitlab.vieon.vn/data/scylla_cluster) | ![No badge](https://gitlab.vieon.vn/data/scylla_cluster/badges/master/coverage.svg) |
| [vieon-services / concurrent-screen-socket](https://gitlab.vieon.vn/vieon-services/concurrent-screen-socket) | ![No badge](https://gitlab.vieon.vn/vieon-services/concurrent-screen-socket/badges/master/coverage.svg) |
| [vieon-platforms / Live-Event-Management](https://gitlab.vieon.vn/vieon-platforms/live-event-management) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/live-event-management/badges/master/coverage.svg) |
| [Artificial Intelligent / AI-visualization-dashboard](https://gitlab.vieon.vn/artificial-intelligent/ai-visualization-dashboard) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/ai-visualization-dashboard/badges/master/coverage.svg) |
| [Artificial Intelligent / transcribe-worker](https://gitlab.vieon.vn/artificial-intelligent/transcribe-worker) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/transcribe-worker/badges/master/coverage.svg) |
| [platform / trigger-pipeline](https://gitlab.vieon.vn/platform/trigger-pipeline) | ![No badge](https://gitlab.vieon.vn/platform/trigger-pipeline/badges/master/coverage.svg) |
| [vieon-services / daemon-retranscode](https://gitlab.vieon.vn/vieon-services/daemon-retranscode) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-retranscode/badges/master/coverage.svg) |
| [platform / script_exporter](https://gitlab.vieon.vn/platform/script_exporter) | ![No badge](https://gitlab.vieon.vn/platform/script_exporter/badges/master/coverage.svg) |
| [vieon-aws-service / cm-activity](https://gitlab.vieon.vn/vieon-aws-service/cm-activity) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/cm-activity/badges/master/coverage.svg) |
| [p-group / Web V5](https://gitlab.vieon.vn/p-group/web-v5) | ![No badge](https://gitlab.vieon.vn/p-group/web-v5/badges/master/coverage.svg) |
| [Artificial Intelligent / AI-post-processing](https://gitlab.vieon.vn/artificial-intelligent/ai-post-processing) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/ai-post-processing/badges/master/coverage.svg) |
| [vieon-aws-service / testhook](https://gitlab.vieon.vn/vieon-aws-service/testhook) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/testhook/badges/master/coverage.svg) |
| [data / vieon_recent_watch_caching](https://gitlab.vieon.vn/data/vieon_recent_watch_caching) | ![No badge](https://gitlab.vieon.vn/data/vieon_recent_watch_caching/badges/master/coverage.svg) |
| [data / vieon_recsys_preparing](https://gitlab.vieon.vn/data/vieon_recsys_preparing) | ![No badge](https://gitlab.vieon.vn/data/vieon_recsys_preparing/badges/master/coverage.svg) |
| [p-group / if](https://gitlab.vieon.vn/p-group/if) | ![No badge](https://gitlab.vieon.vn/p-group/if/badges/master/coverage.svg) |
| [vieon-services / playlist-v2](https://gitlab.vieon.vn/vieon-services/playlist-v2) | ![No badge](https://gitlab.vieon.vn/vieon-services/playlist-v2/badges/master/coverage.svg) |
| [platform / ansible](https://gitlab.vieon.vn/platform/ansible) | ![No badge](https://gitlab.vieon.vn/platform/ansible/badges/master/coverage.svg) |
| [Artificial Intelligent / AI-visualization-service](https://gitlab.vieon.vn/artificial-intelligent/ai-visualization-service) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/ai-visualization-service/badges/master/coverage.svg) |
| [vieon-services / shop-backend-admin-api](https://gitlab.vieon.vn/vieon-services/shop-backend-admin-api) | ![No badge](https://gitlab.vieon.vn/vieon-services/shop-backend-admin-api/badges/master/coverage.svg) |
| [vieon-services / shop-backend-api](https://gitlab.vieon.vn/vieon-services/shop-backend-api) | ![No badge](https://gitlab.vieon.vn/vieon-services/shop-backend-api/badges/master/coverage.svg) |
| [vieon-services / shop-backend-admin](https://gitlab.vieon.vn/vieon-services/shop-backend-admin) | ![No badge](https://gitlab.vieon.vn/vieon-services/shop-backend-admin/badges/master/coverage.svg) |
| [legacy / ott / go-kit-example](https://gitlab.vieon.vn/ott/legacy/go-kit-example) | ![No badge](https://gitlab.vieon.vn/ott/legacy/go-kit-example/badges/master/coverage.svg) |
| [platform / sre / kubernetes-prod](https://gitlab.vieon.vn/platform/sre/kubernetes-prod) | ![No badge](https://gitlab.vieon.vn/platform/sre/kubernetes-prod/badges/master/coverage.svg) |
| [vieon-services / playlist-admin](https://gitlab.vieon.vn/vieon-services/playlist-admin) | ![No badge](https://gitlab.vieon.vn/vieon-services/playlist-admin/badges/master/coverage.svg) |
| [platform / gatling-thangqt](https://gitlab.vieon.vn/platform/gatling-thangqt) | ![No badge](https://gitlab.vieon.vn/platform/gatling-thangqt/badges/master/coverage.svg) |
| [Quality Assurance / ott-services / playlist-autotest](https://gitlab.vieon.vn/quality-assurance/ott-services/playlist-autotest) | ![No badge](https://gitlab.vieon.vn/quality-assurance/ott-services/playlist-autotest/badges/master/coverage.svg) |
| [data / hadoop_exporter](https://gitlab.vieon.vn/data/hadoop_exporter) | ![No badge](https://gitlab.vieon.vn/data/hadoop_exporter/badges/master/coverage.svg) |
| [vieon-aws-service / v5-test](https://gitlab.vieon.vn/vieon-aws-service/v5-test) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/v5-test/badges/master/coverage.svg) |
| [Artificial Intelligent / Sensitive content recognition](https://gitlab.vieon.vn/artificial-intelligent/sensitive-content-recognition) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/sensitive-content-recognition/badges/master/coverage.svg) |
| [p-group / Test Trigger](https://gitlab.vieon.vn/p-group/test-trigger) | ![No badge](https://gitlab.vieon.vn/p-group/test-trigger/badges/master/coverage.svg) |
| [vieon-aws-service / test-v5](https://gitlab.vieon.vn/vieon-aws-service/test-v5) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/test-v5/badges/master/coverage.svg) |
| [platform / cdn / cdn](https://gitlab.vieon.vn/platform/cdn/cdn) | ![No badge](https://gitlab.vieon.vn/platform/cdn/cdn/badges/master/coverage.svg) |
| [data / Api Search VieZone](https://gitlab.vieon.vn/data/api_search_VieZone) | ![No badge](https://gitlab.vieon.vn/data/api_search_VieZone/badges/master/coverage.svg) |
| [vieon-services / Aws Lambda Go](https://gitlab.vieon.vn/vieon-services/aws-lambda-go) | ![No badge](https://gitlab.vieon.vn/vieon-services/aws-lambda-go/badges/master/coverage.svg) |
| [data / Recommend Cf Engine](https://gitlab.vieon.vn/data/recommend_cf_engine) | ![No badge](https://gitlab.vieon.vn/data/recommend_cf_engine/badges/master/coverage.svg) |
| [vieon-services / telco-vnpt](https://gitlab.vieon.vn/vieon-services/telco-vnpt) | ![No badge](https://gitlab.vieon.vn/vieon-services/telco-vnpt/badges/master/coverage.svg) |
| [vieon-aws-service / interactive-game-socket](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-socket) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-socket/badges/master/coverage.svg) |
| [vieon-aws-service / live comment](https://gitlab.vieon.vn/vieon-aws-service/live-comment) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/live-comment/badges/master/coverage.svg) |
| [vieon-services / user-upload](https://gitlab.vieon.vn/vieon-services/user-upload) | ![No badge](https://gitlab.vieon.vn/vieon-services/user-upload/badges/master/coverage.svg) |
| [vieon-aws-service / interactive-game-admin](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-admin) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-admin/badges/master/coverage.svg) |
| [vieon-aws-service / interactive-game-api](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-api) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/interactive-game-api/badges/master/coverage.svg) |
| [Quality Assurance / Xoá số testing](https://gitlab.vieon.vn/quality-assurance/xoa-so-testing) | ![No badge](https://gitlab.vieon.vn/quality-assurance/xoa-so-testing/badges/master/coverage.svg) |
| [vieon-services / go-design-patterns](https://gitlab.vieon.vn/vieon-services/go-design-patterns) | ![No badge](https://gitlab.vieon.vn/vieon-services/go-design-patterns/badges/master/coverage.svg) |
| [Nguyên Minh Duy / Alertmanager Telegram](https://gitlab.vieon.vn/duy.nguyen.minh/alertmanager_telegram) | ![No badge](https://gitlab.vieon.vn/duy.nguyen.minh/alertmanager_telegram/badges/master/coverage.svg) |
| [Artificial Intelligent / Facial Recognition Timesheet Service](https://gitlab.vieon.vn/artificial-intelligent/facial-recognition-timesheet-service) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/facial-recognition-timesheet-service/badges/master/coverage.svg) |
| [Artificial Intelligent / Facial Recognition Timesheet App](https://gitlab.vieon.vn/artificial-intelligent/facial-recognition-timesheet-app) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/facial-recognition-timesheet-app/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / Content Summary Dashboard](https://gitlab.vieon.vn/ott/artificial-intelligent/content-summary-dashboard) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/content-summary-dashboard/badges/master/coverage.svg) |
| [vieon-services / go-dash](https://gitlab.vieon.vn/vieon-services/go-dash) | ![No badge](https://gitlab.vieon.vn/vieon-services/go-dash/badges/master/coverage.svg) |
| [vieon-services / go-hls](https://gitlab.vieon.vn/vieon-services/go-hls) | ![No badge](https://gitlab.vieon.vn/vieon-services/go-hls/badges/master/coverage.svg) |
| [data / vieon_watch_recommend](https://gitlab.vieon.vn/data/vieon_watch_recommend) | ![No badge](https://gitlab.vieon.vn/data/vieon_watch_recommend/badges/master/coverage.svg) |
| [vieon-aws-service / socket](https://gitlab.vieon.vn/vieon-aws-service/socket) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/socket/badges/master/coverage.svg) |
| [data / vieon_watched_transform](https://gitlab.vieon.vn/data/vieon_watched_transform) | ![No badge](https://gitlab.vieon.vn/data/vieon_watched_transform/badges/master/coverage.svg) |
| [data / vieon_trending_ribbon_preparing](https://gitlab.vieon.vn/data/vieon_trending_ribbon_preparing) | ![No badge](https://gitlab.vieon.vn/data/vieon_trending_ribbon_preparing/badges/master/coverage.svg) |
| [vieon-services / live-event-res-ctlr](https://gitlab.vieon.vn/vieon-services/live-event-res-ctlr) | ![No badge](https://gitlab.vieon.vn/vieon-services/live-event-res-ctlr/badges/master/coverage.svg) |
| [vieon-services / interactive-game-socket](https://gitlab.vieon.vn/vieon-services/interactive-game-socket) | ![No badge](https://gitlab.vieon.vn/vieon-services/interactive-game-socket/badges/master/coverage.svg) |
| [vieon-services / Live Comment](https://gitlab.vieon.vn/vieon-services/live-comment) | ![No badge](https://gitlab.vieon.vn/vieon-services/live-comment/badges/master/coverage.svg) |
| [vieon-services / cm-activity](https://gitlab.vieon.vn/vieon-services/cm-activity) | ![No badge](https://gitlab.vieon.vn/vieon-services/cm-activity/badges/master/coverage.svg) |
| [vieon-services / fake-api](https://gitlab.vieon.vn/vieon-services/fake-api) | ![No badge](https://gitlab.vieon.vn/vieon-services/fake-api/badges/master/coverage.svg) |
| [data / deamon_go_track_watch_event](https://gitlab.vieon.vn/data/deamon_go_track_watch_event) | ![No badge](https://gitlab.vieon.vn/data/deamon_go_track_watch_event/badges/master/coverage.svg) |
| [vieon-services / interactive-game-admin](https://gitlab.vieon.vn/vieon-services/interactive-game-admin) | ![No badge](https://gitlab.vieon.vn/vieon-services/interactive-game-admin/badges/master/coverage.svg) |
| [vieon-services / interactive-game-api](https://gitlab.vieon.vn/vieon-services/interactive-game) | ![No badge](https://gitlab.vieon.vn/vieon-services/interactive-game/badges/master/coverage.svg) |
| [vieon-services / api-template](https://gitlab.vieon.vn/vieon-services/api-template) | ![No badge](https://gitlab.vieon.vn/vieon-services/api-template/badges/master/coverage.svg) |
| [vieon-platforms / task-pipeline](https://gitlab.vieon.vn/vieon-platforms/task-pipeline) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/task-pipeline/badges/master/coverage.svg) |
| [Artificial Intelligent / Common Object Detection](https://gitlab.vieon.vn/artificial-intelligent/common-object-detection) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/common-object-detection/badges/master/coverage.svg) |
| [vieon-platforms / showcase](https://gitlab.vieon.vn/vieon-platforms/showcase) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/showcase/badges/master/coverage.svg) |
| [Artificial Intelligent / Keyword Extractor](https://gitlab.vieon.vn/artificial-intelligent/keyword-extractor) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/keyword-extractor/badges/master/coverage.svg) |
| [linh.thai / Admin Ads](https://gitlab.vieon.vn/linh.thai/admin-ads) | ![No badge](https://gitlab.vieon.vn/linh.thai/admin-ads/badges/master/coverage.svg) |
| [vieon-platforms / admin-ads](https://gitlab.vieon.vn/vieon-platforms/admin-ads) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/admin-ads/badges/master/coverage.svg) |
| [Nam Nguyễn / nam-vng-playlist](https://gitlab.vieon.vn/nam.nguyen.tuan/nam-vng-playlist) | ![No badge](https://gitlab.vieon.vn/nam.nguyen.tuan/nam-vng-playlist/badges/master/coverage.svg) |
| [platform / haproxy-develop](https://gitlab.vieon.vn/platform/haproxy-develop) | ![No badge](https://gitlab.vieon.vn/platform/haproxy-develop/badges/master/coverage.svg) |
| [platform / nginx-develop](https://gitlab.vieon.vn/platform/nginx-develop) | ![No badge](https://gitlab.vieon.vn/platform/nginx-develop/badges/master/coverage.svg) |
| [vieon-services / API Admin Live Event](https://gitlab.vieon.vn/vieon-services/api-admin-live-event) | ![No badge](https://gitlab.vieon.vn/vieon-services/api-admin-live-event/badges/master/coverage.svg) |
| [data / Deamon Go Ccu Watch Dt Pro](https://gitlab.vieon.vn/data/deamon-go-ccu-watch-dt-pro) | ![No badge](https://gitlab.vieon.vn/data/deamon-go-ccu-watch-dt-pro/badges/master/coverage.svg) |
| [vieon-services / Go MPD](https://gitlab.vieon.vn/vieon-services/mpd) | ![No badge](https://gitlab.vieon.vn/vieon-services/mpd/badges/master/coverage.svg) |
| [Võ Quốc Cường / imply_cluster](https://gitlab.vieon.vn/cuong.quoc.vo/imply_cluster) | ![No badge](https://gitlab.vieon.vn/cuong.quoc.vo/imply_cluster/badges/master/coverage.svg) |
| [data / deamon-ccu-stream-imply](https://gitlab.vieon.vn/data/deamon-ccu-stream-imply) | ![No badge](https://gitlab.vieon.vn/data/deamon-ccu-stream-imply/badges/master/coverage.svg) |
| [data / vieon_backend_sms_transformer](https://gitlab.vieon.vn/data/vieon_backend_sms_transformer) | ![No badge](https://gitlab.vieon.vn/data/vieon_backend_sms_transformer/badges/master/coverage.svg) |
| [Huy Đỗ / K8s](https://gitlab.vieon.vn/huy.do/k8s) | ![No badge](https://gitlab.vieon.vn/huy.do/k8s/badges/master/coverage.svg) |
| [Artificial Intelligent / video-scene-detection ](https://gitlab.vieon.vn/artificial-intelligent/video-scene-detection) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/video-scene-detection/badges/master/coverage.svg) |
| [p-group / workloads-dev](https://gitlab.vieon.vn/p-group/workloads-dev) | ![No badge](https://gitlab.vieon.vn/p-group/workloads-dev/badges/master/coverage.svg) |
| [thang-test-project / test-hook](https://gitlab.vieon.vn/thang-test-project/test-hook) | ![No badge](https://gitlab.vieon.vn/thang-test-project/test-hook/badges/master/coverage.svg) |
| [vieon-services / backend-api-admin](https://gitlab.vieon.vn/vieon-services/backend-api-admin) | ![No badge](https://gitlab.vieon.vn/vieon-services/backend-api-admin/badges/master/coverage.svg) |
| [platform / cdn / cdn-dns](https://gitlab.vieon.vn/platform/cdn/cdn-dns) | ![No badge](https://gitlab.vieon.vn/platform/cdn/cdn-dns/badges/master/coverage.svg) |
| [platform / K8s-all-in-1](https://gitlab.vieon.vn/platform/k8s-all-in-1) | ![No badge](https://gitlab.vieon.vn/platform/k8s-all-in-1/badges/master/coverage.svg) |
| [vieon-services / base-services-images](https://gitlab.vieon.vn/vieon-services/base-services-images) | ![No badge](https://gitlab.vieon.vn/vieon-services/base-services-images/badges/master/coverage.svg) |
| [Nam Nguyễn / service-images-base](https://gitlab.vieon.vn/nam.nguyen.tuan/service-images-base) | ![No badge](https://gitlab.vieon.vn/nam.nguyen.tuan/service-images-base/badges/master/coverage.svg) |
| [platform / api-dns](https://gitlab.vieon.vn/platform/gdnsd) | ![No badge](https://gitlab.vieon.vn/platform/gdnsd/badges/master/coverage.svg) |
| [Viezone / VZ Player](https://gitlab.vieon.vn/viezone/vz-player) | ![No badge](https://gitlab.vieon.vn/viezone/vz-player/badges/master/coverage.svg) |
| [DevOps / Test](https://gitlab.vieon.vn/devops/test) | ![No badge](https://gitlab.vieon.vn/devops/test/badges/master/coverage.svg) |
| [data / api-search-suggestion](https://gitlab.vieon.vn/data/api-search-suggestion) | ![No badge](https://gitlab.vieon.vn/data/api-search-suggestion/badges/master/coverage.svg) |
| [Huy Đỗ / huydo.test](https://gitlab.vieon.vn/huy.do/huydo.test) | ![No badge](https://gitlab.vieon.vn/huy.do/huydo.test/badges/master/coverage.svg) |
| [Nam Nguyễn / nam](https://gitlab.vieon.vn/nam.nguyen.tuan/nam) | ![No badge](https://gitlab.vieon.vn/nam.nguyen.tuan/nam/badges/master/coverage.svg) |
| [Nguyên Minh Duy / Clear cache CDN-DEV](https://gitlab.vieon.vn/duy.nguyen.minh/clear-cache-cdn-dev) | ![No badge](https://gitlab.vieon.vn/duy.nguyen.minh/clear-cache-cdn-dev/badges/master/coverage.svg) |
| [vieon-services / report-kplus](https://gitlab.vieon.vn/vieon-services/report-kplus) | ![No badge](https://gitlab.vieon.vn/vieon-services/report-kplus/badges/master/coverage.svg) |
| [legacy / ott / daemon-ott-queues-migrate-data](https://gitlab.vieon.vn/ott/legacy/daemon-ott-queues-migrate-data) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-ott-queues-migrate-data/badges/master/coverage.svg) |
| [legacy / ott / daemon-ott-precache](https://gitlab.vieon.vn/ott/legacy/daemon-ott-precache) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-ott-precache/badges/master/coverage.svg) |
| [legacy / ott / daemon-go-watch](https://gitlab.vieon.vn/ott/legacy/daemon-go-watch) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-go-watch/badges/master/coverage.svg) |
| [legacy / ott / daemon-go-ccu](https://gitlab.vieon.vn/ott/legacy/daemon-go-ccu) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-go-ccu/badges/master/coverage.svg) |
| [vieon-services / daemon-ott-job](https://gitlab.vieon.vn/vieon-services/daemon-ott-job) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-ott-job/badges/master/coverage.svg) |
| [legacy / ott / daemon-go-parse-logs](https://gitlab.vieon.vn/ott/legacy/daemon-go-parse-logs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-go-parse-logs/badges/master/coverage.svg) |
| [vieon-services / daemon-go-raw-logs](https://gitlab.vieon.vn/vieon-services/daemon-go-raw-logs) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-go-raw-logs/badges/master/coverage.svg) |
| [p-group / playlist-aws](https://gitlab.vieon.vn/p-group/playlist-aws) | ![No badge](https://gitlab.vieon.vn/p-group/playlist-aws/badges/master/coverage.svg) |
| [vieon-services / geo-service](https://gitlab.vieon.vn/vieon-services/geo-service) | ![No badge](https://gitlab.vieon.vn/vieon-services/geo-service/badges/master/coverage.svg) |
| [vieon-aws-service / cm_v3](https://gitlab.vieon.vn/vieon-aws-service/cm_v3) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/cm_v3/badges/master/coverage.svg) |
| [vieon-aws-service / backend-user-active](https://gitlab.vieon.vn/vieon-aws-service/backend-user-active) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/backend-user-active/badges/master/coverage.svg) |
| [vieon-aws-service / playlist](https://gitlab.vieon.vn/vieon-aws-service/playlist) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/playlist/badges/master/coverage.svg) |
| [p-group / playlist](https://gitlab.vieon.vn/p-group/playlist) | ![No badge](https://gitlab.vieon.vn/p-group/playlist/badges/master/coverage.svg) |
| [vieon-services / api-admin-ads](https://gitlab.vieon.vn/vieon-services/api-admin-ads) | ![No badge](https://gitlab.vieon.vn/vieon-services/api-admin-ads/badges/master/coverage.svg) |
| [p-group / cm-v5](https://gitlab.vieon.vn/p-group/cm-v5) | ![No badge](https://gitlab.vieon.vn/p-group/cm-v5/badges/master/coverage.svg) |
| [vieon-aws-service / backend-user](https://gitlab.vieon.vn/vieon-aws-service/backend-user) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/backend-user/badges/master/coverage.svg) |
| [vieon-aws-service / cm_v5](https://gitlab.vieon.vn/vieon-aws-service/cm_v5) | ![No badge](https://gitlab.vieon.vn/vieon-aws-service/cm_v5/badges/master/coverage.svg) |
| [data / trigger](https://gitlab.vieon.vn/data/trigger) | ![No badge](https://gitlab.vieon.vn/data/trigger/badges/master/coverage.svg) |
| [data / Testsite](https://gitlab.vieon.vn/data/testsite) | ![No badge](https://gitlab.vieon.vn/data/testsite/badges/master/coverage.svg) |
| [vieon-services / backend-admin](https://gitlab.vieon.vn/vieon-services/backend-admin) | ![No badge](https://gitlab.vieon.vn/vieon-services/backend-admin/badges/master/coverage.svg) |
| [vieon-services / task-pipeline](https://gitlab.vieon.vn/vieon-services/task-pipeline) | ![No badge](https://gitlab.vieon.vn/vieon-services/task-pipeline/badges/master/coverage.svg) |
| [platform / vieon-gitops](https://gitlab.vieon.vn/platform/vieon-gitops) | ![No badge](https://gitlab.vieon.vn/platform/vieon-gitops/badges/master/coverage.svg) |
| [vieon-services / backend-user](https://gitlab.vieon.vn/vieon-services/backend-user) | ![No badge](https://gitlab.vieon.vn/vieon-services/backend-user/badges/master/coverage.svg) |
| [vieon-services / backend-user-active](https://gitlab.vieon.vn/vieon-services/backend-user-active) | ![No badge](https://gitlab.vieon.vn/vieon-services/backend-user-active/badges/master/coverage.svg) |
| [Viezone / Instant Article](https://gitlab.vieon.vn/viezone/instant-article) | ![No badge](https://gitlab.vieon.vn/viezone/instant-article/badges/master/coverage.svg) |
| [vieon-templates / test-ci-template](https://gitlab.vieon.vn/vieon-templates/test-ci-template) | ![No badge](https://gitlab.vieon.vn/vieon-templates/test-ci-template/badges/master/coverage.svg) |
| [vieon-services / Log Tracking Watch](https://gitlab.vieon.vn/vieon-services/tracking-watch-log) | ![No badge](https://gitlab.vieon.vn/vieon-services/tracking-watch-log/badges/master/coverage.svg) |
| [vieon-sdk / analytics-node-sdk](https://gitlab.vieon.vn/vieon-sdk/analytics-node-sdk) | ![No badge](https://gitlab.vieon.vn/vieon-sdk/analytics-node-sdk/badges/master/coverage.svg) |
| [vieon-sdk / analytics-android-sdk](https://gitlab.vieon.vn/vieon-sdk/analytics-android-sdk) | ![No badge](https://gitlab.vieon.vn/vieon-sdk/analytics-android-sdk/badges/master/coverage.svg) |
| [vieon-sdk / analytics-ios-sdk](https://gitlab.vieon.vn/vieon-sdk/analytics-ios-sdk) | ![No badge](https://gitlab.vieon.vn/vieon-sdk/analytics-ios-sdk/badges/master/coverage.svg) |
| [Viezone / Rabbitmq Service](https://gitlab.vieon.vn/viezone/rabbitmq-service) | ![No badge](https://gitlab.vieon.vn/viezone/rabbitmq-service/badges/master/coverage.svg) |
| [Thang Nguyen / VieON SDK iOS](https://gitlab.vieon.vn/thang.nguyen.minh/vieon-sdk-ios) | ![No badge](https://gitlab.vieon.vn/thang.nguyen.minh/vieon-sdk-ios/badges/master/coverage.svg) |
| [Artificial Intelligent / VOD Automation QC](https://gitlab.vieon.vn/artificial-intelligent/vod-automation-qc) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/vod-automation-qc/badges/master/coverage.svg) |
| [platform / perftest](https://gitlab.vieon.vn/platform/perftest) | ![No badge](https://gitlab.vieon.vn/platform/perftest/badges/master/coverage.svg) |
| [Artificial Intelligent / Scene-Recognition](https://gitlab.vieon.vn/artificial-intelligent/scene-recognition) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/scene-recognition/badges/master/coverage.svg) |
| [data / sync-updated-content-ribbons](https://gitlab.vieon.vn/data/sync-updated-content-ribbons) | ![No badge](https://gitlab.vieon.vn/data/sync-updated-content-ribbons/badges/master/coverage.svg) |
| [platform / geoip2](https://gitlab.vieon.vn/platform/geoip2) | ![No badge](https://gitlab.vieon.vn/platform/geoip2/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / dashboard-ai-management](https://gitlab.vieon.vn/ott/artificial-intelligent/dashboard-ai-management) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/dashboard-ai-management/badges/master/coverage.svg) |
| [data / api-search-engine](https://gitlab.vieon.vn/data/api-search-engine) | ![No badge](https://gitlab.vieon.vn/data/api-search-engine/badges/master/coverage.svg) |
| [platform / Loadtest](https://gitlab.vieon.vn/platform/loadtest) | ![No badge](https://gitlab.vieon.vn/platform/loadtest/badges/master/coverage.svg) |
| [data / transfer-livetv](https://gitlab.vieon.vn/data/transfer-livetv) | ![No badge](https://gitlab.vieon.vn/data/transfer-livetv/badges/master/coverage.svg) |
| [Ma Thiên Phúc / cm-v5](https://gitlab.vieon.vn/phuc.ma/cm-v5) | ![No badge](https://gitlab.vieon.vn/phuc.ma/cm-v5/badges/master/coverage.svg) |
| [data / transfer_kafka_request_logs](https://gitlab.vieon.vn/data/transfer_kafka_request_logs) | ![No badge](https://gitlab.vieon.vn/data/transfer_kafka_request_logs/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / reporter-service](https://gitlab.vieon.vn/ott/artificial-intelligent/reporter-service) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/reporter-service/badges/master/coverage.svg) |
| [data / airflow_recommend](https://gitlab.vieon.vn/data/airflow_recommend) | ![No badge](https://gitlab.vieon.vn/data/airflow_recommend/badges/master/coverage.svg) |
| [data / recommendation_engine](https://gitlab.vieon.vn/data/recommendation_engine) | ![No badge](https://gitlab.vieon.vn/data/recommendation_engine/badges/master/coverage.svg) |
| [Võ Quốc Cường / vieon_recsys_nearline](https://gitlab.vieon.vn/cuong.quoc.vo/vieon_recsys_nearline) | ![No badge](https://gitlab.vieon.vn/cuong.quoc.vo/vieon_recsys_nearline/badges/master/coverage.svg) |
| [legacy / ott / Android React Native](https://gitlab.vieon.vn/ott/legacy/vieplay-rn-new) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieplay-rn-new/badges/master/coverage.svg) |
| [data / api-reccomend-engine](https://gitlab.vieon.vn/data/api-reccomend-engine) | ![No badge](https://gitlab.vieon.vn/data/api-reccomend-engine/badges/master/coverage.svg) |
| [vieon-services / partner-apis](https://gitlab.vieon.vn/vieon-services/partner-apis) | ![No badge](https://gitlab.vieon.vn/vieon-services/partner-apis/badges/master/coverage.svg) |
| [legacy / ott / devices-ott-mobile-web-nextjs](https://gitlab.vieon.vn/ott/legacy/devices-ott-mobile-web-nextjs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-ott-mobile-web-nextjs/badges/master/coverage.svg) |
| [data / transfer_kafka_watched_logs](https://gitlab.vieon.vn/data/transfer_kafka_watched_logs) | ![No badge](https://gitlab.vieon.vn/data/transfer_kafka_watched_logs/badges/master/coverage.svg) |
| [Artificial Intelligent / Branding Recognition](https://gitlab.vieon.vn/artificial-intelligent/branding-recognition) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/branding-recognition/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / action-recognition](https://gitlab.vieon.vn/ott/artificial-intelligent/action-recognition) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/action-recognition/badges/master/coverage.svg) |
| [Ma Thiên Phúc / devices-viehub-web-v4-h](https://gitlab.vieon.vn/phuc.ma/devices-viehub-web-v4-h) | ![No badge](https://gitlab.vieon.vn/phuc.ma/devices-viehub-web-v4-h/badges/master/coverage.svg) |
| [platform / livetv_exporter](https://gitlab.vieon.vn/platform/livetv_exporter) | ![No badge](https://gitlab.vieon.vn/platform/livetv_exporter/badges/master/coverage.svg) |
| [legacy / ott / daemon-yusp-recommendation-sync-data](https://gitlab.vieon.vn/ott/legacy/daemon-yusp-recommendation-sync-data) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-yusp-recommendation-sync-data/badges/master/coverage.svg) |
| [legacy / ott / daemon-yusp-recommendation-tracking-data](https://gitlab.vieon.vn/ott/legacy/daemon-yusp-recommendation-tracking-data) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-yusp-recommendation-tracking-data/badges/master/coverage.svg) |
| [Artificial Intelligent / VN-celeb-face-vs-emotion](https://gitlab.vieon.vn/artificial-intelligent/vn-celeb-face-vs-emotion) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/vn-celeb-face-vs-emotion/badges/master/coverage.svg) |
| [Quality Assurance / vieon-auto-web](https://gitlab.vieon.vn/quality-assurance/vieon-auto-web) | ![No badge](https://gitlab.vieon.vn/quality-assurance/vieon-auto-web/badges/master/coverage.svg) |
| [platform / ffmpeg](https://gitlab.vieon.vn/platform/ffmpeg) | ![No badge](https://gitlab.vieon.vn/platform/ffmpeg/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / ai-video-indexing-api](https://gitlab.vieon.vn/ott/artificial-intelligent/ai-video-indexing-api) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/ai-video-indexing-api/badges/master/coverage.svg) |
| [data / history-event-cs](https://gitlab.vieon.vn/data/history-event-cs) | ![No badge](https://gitlab.vieon.vn/data/history-event-cs/badges/master/coverage.svg) |
| [Artificial Intelligent / poster-candidate](https://gitlab.vieon.vn/artificial-intelligent/poster-candidate) | ![No badge](https://gitlab.vieon.vn/artificial-intelligent/poster-candidate/badges/master/coverage.svg) |
| [vieon-services / service-init-cache](https://gitlab.vieon.vn/vieon-services/service-init-cache) | ![No badge](https://gitlab.vieon.vn/vieon-services/service-init-cache/badges/master/coverage.svg) |
| [Nguyễn Minh Tiến / service-init-cache](https://gitlab.vieon.vn/tien.nguyen/service-init-cache) | ![No badge](https://gitlab.vieon.vn/tien.nguyen/service-init-cache/badges/master/coverage.svg) |
| [vieon-services / interface-common](https://gitlab.vieon.vn/vieon-services/interface-common) | ![No badge](https://gitlab.vieon.vn/vieon-services/interface-common/badges/master/coverage.svg) |
| [data / etl-historical-data-es](https://gitlab.vieon.vn/data/etl-historical-data-es) | ![No badge](https://gitlab.vieon.vn/data/etl-historical-data-es/badges/master/coverage.svg) |
| [vieon-services / daemon-crow-job](https://gitlab.vieon.vn/vieon-services/daemon-crow-job) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-crow-job/badges/master/coverage.svg) |
| [Võ Quốc Cường / zeppelin_notebooks](https://gitlab.vieon.vn/cuong.quoc.vo/zeppelin_notebooks) | ![No badge](https://gitlab.vieon.vn/cuong.quoc.vo/zeppelin_notebooks/badges/master/coverage.svg) |
| [data / sync-account-user-postgre-redis](https://gitlab.vieon.vn/data/sync-account-user-postgre-redis) | ![No badge](https://gitlab.vieon.vn/data/sync-account-user-postgre-redis/badges/master/coverage.svg) |
| [data / deamon-go-log-datateam-cs](https://gitlab.vieon.vn/data/deamon-go-log-datateam-cms) | ![No badge](https://gitlab.vieon.vn/data/deamon-go-log-datateam-cms/badges/master/coverage.svg) |
| [vieon-services / ott-telco-h](https://gitlab.vieon.vn/vieon-services/ott-telco-h) | ![No badge](https://gitlab.vieon.vn/vieon-services/ott-telco-h/badges/master/coverage.svg) |
| [legacy / ott / live2vod-daemon](https://gitlab.vieon.vn/ott/legacy/live2vod-daemon) | ![No badge](https://gitlab.vieon.vn/ott/legacy/live2vod-daemon/badges/master/coverage.svg) |
| [vieon-services / services-ott-telco](https://gitlab.vieon.vn/vieon-services/services-ott-telco) | ![No badge](https://gitlab.vieon.vn/vieon-services/services-ott-telco/badges/master/coverage.svg) |
| [Lê Văn Duẩn / deamon_go_ccu_vip_pro](https://gitlab.vieon.vn/duan.le/deamon_go_ccu_vip_pro) | ![No badge](https://gitlab.vieon.vn/duan.le/deamon_go_ccu_vip_pro/badges/master/coverage.svg) |
| [data / airflow_cluster](https://gitlab.vieon.vn/data/airflow_cluster) | ![No badge](https://gitlab.vieon.vn/data/airflow_cluster/badges/master/coverage.svg) |
| [Quality Assurance / VieON_Auto_App](https://gitlab.vieon.vn/quality-assurance/vieon_auto_app) | ![No badge](https://gitlab.vieon.vn/quality-assurance/vieon_auto_app/badges/master/coverage.svg) |
| [legacy / helmlab](https://gitlab.vieon.vn/ott/helmlab) | ![No badge](https://gitlab.vieon.vn/ott/helmlab/badges/master/coverage.svg) |
| [Quality Assurance / qrcode-video](https://gitlab.vieon.vn/quality-assurance/qrcode-video) | ![No badge](https://gitlab.vieon.vn/quality-assurance/qrcode-video/badges/master/coverage.svg) |
| [vieon-services / daemon-crawler-match](https://gitlab.vieon.vn/vieon-services/daemon-crawler-match) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-crawler-match/badges/master/coverage.svg) |
| [Viezone / Frontend API](https://gitlab.vieon.vn/viezone/frontend-api) | ![No badge](https://gitlab.vieon.vn/viezone/frontend-api/badges/master/coverage.svg) |
| [vieon-platforms / source-validator](https://gitlab.vieon.vn/vieon-platforms/source-validator) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/source-validator/badges/master/coverage.svg) |
| [vieon-templates / ci-template](https://gitlab.vieon.vn/vieon-templates/ci-template) | ![No badge](https://gitlab.vieon.vn/vieon-templates/ci-template/badges/master/coverage.svg) |
| [legacy / ott / log-tracking](https://gitlab.vieon.vn/ott/legacy/log-tracking) | ![No badge](https://gitlab.vieon.vn/ott/legacy/log-tracking/badges/master/coverage.svg) |
| [Nguyên Minh Duy / multi-master-kubernetes](https://gitlab.vieon.vn/duy.nguyen.minh/multi-master-kubernetes) | ![No badge](https://gitlab.vieon.vn/duy.nguyen.minh/multi-master-kubernetes/badges/master/coverage.svg) |
| [vieon-services / service-migrate-data](https://gitlab.vieon.vn/vieon-services/service-migrate-data) | ![No badge](https://gitlab.vieon.vn/vieon-services/service-migrate-data/badges/master/coverage.svg) |
| [vieon-platforms / html-tv-v5](https://gitlab.vieon.vn/vieon-platforms/html-tv-v5) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/html-tv-v5/badges/master/coverage.svg) |
| [vieon-services / interface-personal-message](https://gitlab.vieon.vn/vieon-services/interface-personal-message) | ![No badge](https://gitlab.vieon.vn/vieon-services/interface-personal-message/badges/master/coverage.svg) |
| [vieon-platforms / html-email](https://gitlab.vieon.vn/vieon-platforms/html-email) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/html-email/badges/master/coverage.svg) |
| [Viezone / Frontend](https://gitlab.vieon.vn/viezone/frontend) | ![No badge](https://gitlab.vieon.vn/viezone/frontend/badges/master/coverage.svg) |
| [Viezone / CMS](https://gitlab.vieon.vn/viezone/cms) | ![No badge](https://gitlab.vieon.vn/viezone/cms/badges/master/coverage.svg) |
| [Viezone / Content API](https://gitlab.vieon.vn/viezone/content-api) | ![No badge](https://gitlab.vieon.vn/viezone/content-api/badges/master/coverage.svg) |
| [Ma Thiên Phúc / devices-viehub-web-v4-demo](https://gitlab.vieon.vn/phuc.ma/devices-viehub-web-v4-demo) | ![No badge](https://gitlab.vieon.vn/phuc.ma/devices-viehub-web-v4-demo/badges/master/coverage.svg) |
| [vieon-platforms / Web V5](https://gitlab.vieon.vn/vieon-platforms/web-v5) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/web-v5/badges/master/coverage.svg) |
| [platform / cdn / cdn-init](https://gitlab.vieon.vn/platform/cdn/cdn-init) | ![No badge](https://gitlab.vieon.vn/platform/cdn/cdn-init/badges/master/coverage.svg) |
| [vieon-services / User Report](https://gitlab.vieon.vn/vieon-services/user-report) | ![No badge](https://gitlab.vieon.vn/vieon-services/user-report/badges/master/coverage.svg) |
| [platform / kubernetes / deployment](https://gitlab.vieon.vn/platform/kubernetes/deployment) | ![No badge](https://gitlab.vieon.vn/platform/kubernetes/deployment/badges/master/coverage.svg) |
| [Lê Văn Duẩn / etl_external_data_gam360](https://gitlab.vieon.vn/duan.le/etl_external_data_gam360) | ![No badge](https://gitlab.vieon.vn/duan.le/etl_external_data_gam360/badges/master/coverage.svg) |
| [Lê Văn Duẩn / etl_historical_data_es](https://gitlab.vieon.vn/duan.le/etl_historical_data_es) | ![No badge](https://gitlab.vieon.vn/duan.le/etl_historical_data_es/badges/master/coverage.svg) |
| [Ma Thiên Phúc / My static website](https://gitlab.vieon.vn/phuc.ma/my-static-website) | ![No badge](https://gitlab.vieon.vn/phuc.ma/my-static-website/badges/master/coverage.svg) |
| [Ma Thiên Phúc / Test](https://gitlab.vieon.vn/phuc.ma/test) | ![No badge](https://gitlab.vieon.vn/phuc.ma/test/badges/master/coverage.svg) |
| [vieon-services / sitemap](https://gitlab.vieon.vn/vieon-services/sitemap) | ![No badge](https://gitlab.vieon.vn/vieon-services/sitemap/badges/master/coverage.svg) |
| [vieon-services / send-notify](https://gitlab.vieon.vn/vieon-services/send-notify) | ![No badge](https://gitlab.vieon.vn/vieon-services/send-notify/badges/master/coverage.svg) |
| [vieon-services / send-sms](https://gitlab.vieon.vn/vieon-services/send-sms) | ![No badge](https://gitlab.vieon.vn/vieon-services/send-sms/badges/master/coverage.svg) |
| [vieon-services / send-mail](https://gitlab.vieon.vn/vieon-services/send-mail) | ![No badge](https://gitlab.vieon.vn/vieon-services/send-mail/badges/master/coverage.svg) |
| [data / artifacts / imply](https://gitlab.vieon.vn/data/artifacts/imply) | ![No badge](https://gitlab.vieon.vn/data/artifacts/imply/badges/master/coverage.svg) |
| [data / artifacts / hadoop](https://gitlab.vieon.vn/data/artifacts/hadoop) | ![No badge](https://gitlab.vieon.vn/data/artifacts/hadoop/badges/master/coverage.svg) |
| [data / artifacts / zookeeper](https://gitlab.vieon.vn/data/artifacts/zookeeper) | ![No badge](https://gitlab.vieon.vn/data/artifacts/zookeeper/badges/master/coverage.svg) |
| [data / bigdata-artifact](https://gitlab.vieon.vn/data/bigdata-artifact) | ![No badge](https://gitlab.vieon.vn/data/bigdata-artifact/badges/master/coverage.svg) |
| [Giã Dương Đức Minh / adhoc-script](https://gitlab.vieon.vn/minh.gia/adhoc-script) | ![No badge](https://gitlab.vieon.vn/minh.gia/adhoc-script/badges/master/coverage.svg) |
| [vieon-services / playlist-autotest](https://gitlab.vieon.vn/vieon-services/playlist-autotest) | ![No badge](https://gitlab.vieon.vn/vieon-services/playlist-autotest/badges/master/coverage.svg) |
| [data / airflow_contrib](https://gitlab.vieon.vn/data/airflow_contrib) | ![No badge](https://gitlab.vieon.vn/data/airflow_contrib/badges/master/coverage.svg) |
| [legacy / ott / ott-services-giftcode](https://gitlab.vieon.vn/ott/legacy/ott-services-giftcode) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-services-giftcode/badges/master/coverage.svg) |
| [legacy / ott / ott-data-etl-es-jobs](https://gitlab.vieon.vn/ott/legacy/ott-data-etl-es-jobs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-data-etl-es-jobs/badges/master/coverage.svg) |
| [legacy / ott / ott-daemons-aiotcare-sync-data-crm](https://gitlab.vieon.vn/ott/legacy/ott-daemons-aiotcare-sync-data-crm) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-daemons-aiotcare-sync-data-crm/badges/master/coverage.svg) |
| [legacy / ott / ott-services-campaign](https://gitlab.vieon.vn/ott/legacy/ott-services-campaign) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-services-campaign/badges/master/coverage.svg) |
| [legacy / ott / ott-services-report](https://gitlab.vieon.vn/ott/legacy/ott-services-report) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-services-report/badges/master/coverage.svg) |
| [legacy / ott / devices-ott-mobile-android-native](https://gitlab.vieon.vn/ott/legacy/devices-ott-mobile-android-native) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-ott-mobile-android-native/badges/master/coverage.svg) |
| [legacy / ott / ott-daemons-sitemap](https://gitlab.vieon.vn/ott/legacy/ott-daemons-sitemap) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-daemons-sitemap/badges/master/coverage.svg) |
| [legacy / ott / ott-services-validation](https://gitlab.vieon.vn/ott/legacy/ott-services-validation) | ![No badge](https://gitlab.vieon.vn/ott/legacy/ott-services-validation/badges/master/coverage.svg) |
| [legacy / ott / services-ab-testing](https://gitlab.vieon.vn/ott/legacy/services-ab-testing) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-ab-testing/badges/master/coverage.svg) |
| [legacy / ott / services-admin](https://gitlab.vieon.vn/ott/legacy/services-admin) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-admin/badges/master/coverage.svg) |
| [legacy / ott / me-phim](https://gitlab.vieon.vn/ott/legacy/me-phim) | ![No badge](https://gitlab.vieon.vn/ott/legacy/me-phim/badges/master/coverage.svg) |
| [legacy / ott / devices-ios](https://gitlab.vieon.vn/ott/legacy/devices-ios) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-ios/badges/master/coverage.svg) |
| [legacy / ott / devices-android](https://gitlab.vieon.vn/ott/legacy/devices-android) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-android/badges/master/coverage.svg) |
| [legacy / ott / mobile](https://gitlab.vieon.vn/ott/legacy/mobile) | ![No badge](https://gitlab.vieon.vn/ott/legacy/mobile/badges/master/coverage.svg) |
| [vieon-services / daemons-auto-check](https://gitlab.vieon.vn/vieon-services/daemons-auto-check) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemons-auto-check/badges/master/coverage.svg) |
| [legacy / ott / payments](https://gitlab.vieon.vn/ott/legacy/payments) | ![No badge](https://gitlab.vieon.vn/ott/legacy/payments/badges/master/coverage.svg) |
| [legacy / ott / vieon-tv](https://gitlab.vieon.vn/ott/legacy/vieon-tv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-tv/badges/master/coverage.svg) |
| [Quality Assurance / qcrepo](https://gitlab.vieon.vn/quality-assurance/qcrepo) | ![No badge](https://gitlab.vieon.vn/quality-assurance/qcrepo/badges/master/coverage.svg) |
| [platform / devops_tools](https://gitlab.vieon.vn/platform/devops_tools) | ![No badge](https://gitlab.vieon.vn/platform/devops_tools/badges/master/coverage.svg) |
| [vieon-services / daemon-epg-crawlers](https://gitlab.vieon.vn/vieon-services/daemon-epg-crawlers) | ![No badge](https://gitlab.vieon.vn/vieon-services/daemon-epg-crawlers/badges/master/coverage.svg) |
| [vieon-platforms / Android TV](https://gitlab.vieon.vn/vieon-platforms/android-tv) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/android-tv/badges/master/coverage.svg) |
| [legacy / ott / vieon-web](https://gitlab.vieon.vn/ott/legacy/vieon-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-web/badges/master/coverage.svg) |
| [legacy / ott / grpc-vod](https://gitlab.vieon.vn/ott/legacy/grpc-vod) | ![No badge](https://gitlab.vieon.vn/ott/legacy/grpc-vod/badges/master/coverage.svg) |
| [legacy / ott / mobile-web-nextjs](https://gitlab.vieon.vn/ott/legacy/mobile-web-nextjs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/mobile-web-nextjs/badges/master/coverage.svg) |
| [legacy / ott / vieon-smarttv](https://gitlab.vieon.vn/ott/legacy/vieon-smarttv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-smarttv/badges/master/coverage.svg) |
| [legacy / ott / backend-go-gameshow](https://gitlab.vieon.vn/ott/legacy/backend-go-gameshow) | ![No badge](https://gitlab.vieon.vn/ott/legacy/backend-go-gameshow/badges/master/coverage.svg) |
| [legacy / ott / daemon-queues-migrate-data](https://gitlab.vieon.vn/ott/legacy/daemon-queues-migrate-data) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemon-queues-migrate-data/badges/master/coverage.svg) |
| [legacy / ott / transcode-cms](https://gitlab.vieon.vn/ott/legacy/transcode-cms) | ![No badge](https://gitlab.vieon.vn/ott/legacy/transcode-cms/badges/master/coverage.svg) |
| [legacy / ott / transcode-worker](https://gitlab.vieon.vn/ott/legacy/transcode-worker) | ![No badge](https://gitlab.vieon.vn/ott/legacy/transcode-worker/badges/master/coverage.svg) |
| [legacy / ott / transcode-controller](https://gitlab.vieon.vn/ott/legacy/transcode-controller) | ![No badge](https://gitlab.vieon.vn/ott/legacy/transcode-controller/badges/master/coverage.svg) |
| [legacy / ott / cast_receiver](https://gitlab.vieon.vn/ott/legacy/cast_receiver) | ![No badge](https://gitlab.vieon.vn/ott/legacy/cast_receiver/badges/master/coverage.svg) |
| [vieon-services / backend-v3](https://gitlab.vieon.vn/vieon-services/backend-v3) | ![No badge](https://gitlab.vieon.vn/vieon-services/backend-v3/badges/master/coverage.svg) |
| [legacy / ott / mobile-web](https://gitlab.vieon.vn/ott/legacy/mobile-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/mobile-web/badges/master/coverage.svg) |
| [legacy / ott / plist](https://gitlab.vieon.vn/ott/legacy/plist) | ![No badge](https://gitlab.vieon.vn/ott/legacy/plist/badges/master/coverage.svg) |
| [legacy / ott / admin](https://gitlab.vieon.vn/ott/legacy/admin) | ![No badge](https://gitlab.vieon.vn/ott/legacy/admin/badges/master/coverage.svg) |
| [legacy / ott / vieplay-backend](https://gitlab.vieon.vn/ott/legacy/vieplay-backend) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieplay-backend/badges/master/coverage.svg) |
| [legacy / ott / tv](https://gitlab.vieon.vn/ott/legacy/tv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/tv/badges/master/coverage.svg) |
| [legacy / ott / smarttv](https://gitlab.vieon.vn/ott/legacy/smarttv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/smarttv/badges/master/coverage.svg) |
| [legacy / ott / vieplay-rn](https://gitlab.vieon.vn/ott/legacy/vieplay-rn) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieplay-rn/badges/master/coverage.svg) |
| [legacy / ott / androidtv](https://gitlab.vieon.vn/ott/legacy/androidtv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/androidtv/badges/master/coverage.svg) |
| [legacy / ott / android-tablet](https://gitlab.vieon.vn/ott/legacy/android-tablet) | ![No badge](https://gitlab.vieon.vn/ott/legacy/android-tablet/badges/master/coverage.svg) |
| [platform / devices-tools-deployments](https://gitlab.vieon.vn/platform/devices-tools-deployments) | ![No badge](https://gitlab.vieon.vn/platform/devices-tools-deployments/badges/master/coverage.svg) |
| [platform / tools-deployments](https://gitlab.vieon.vn/platform/tools-deployments) | ![No badge](https://gitlab.vieon.vn/platform/tools-deployments/badges/master/coverage.svg) |
| [legacy / ott / rnd-app](https://gitlab.vieon.vn/ott/legacy/rnd-app) | ![No badge](https://gitlab.vieon.vn/ott/legacy/rnd-app/badges/master/coverage.svg) |
| [legacy / ott / rnd-web](https://gitlab.vieon.vn/ott/legacy/rnd-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/rnd-web/badges/master/coverage.svg) |
| [legacy / ott / vtvcab_connector](https://gitlab.vieon.vn/ott/legacy/vtvcab_connector) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vtvcab_connector/badges/master/coverage.svg) |
| [legacy / ott / viestars](https://gitlab.vieon.vn/ott/legacy/viestars) | ![No badge](https://gitlab.vieon.vn/ott/legacy/viestars/badges/master/coverage.svg) |
| [legacy / ott / vieplay-usc](https://gitlab.vieon.vn/ott/legacy/vieplay-usc) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieplay-usc/badges/master/coverage.svg) |
| [legacy / ott / vieon-ssr-mobile](https://gitlab.vieon.vn/ott/legacy/vieon-ssr-mobile) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-ssr-mobile/badges/master/coverage.svg) |
| [legacy / ott / vieon-ssr](https://gitlab.vieon.vn/ott/legacy/vieon-ssr) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-ssr/badges/master/coverage.svg) |
| [legacy / ott / vieon-report-mail](https://gitlab.vieon.vn/ott/legacy/vieon-report-mail) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-report-mail/badges/master/coverage.svg) |
| [legacy / ott / vieon-player](https://gitlab.vieon.vn/ott/legacy/vieon-player) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-player/badges/master/coverage.svg) |
| [legacy / ott / vieon-dashboard](https://gitlab.vieon.vn/ott/legacy/vieon-dashboard) | ![No badge](https://gitlab.vieon.vn/ott/legacy/vieon-dashboard/badges/master/coverage.svg) |
| [legacy / ott / viehub-web](https://gitlab.vieon.vn/ott/legacy/viehub-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/viehub-web/badges/master/coverage.svg) |
| [legacy / ott / tools-youtube-migrator](https://gitlab.vieon.vn/ott/legacy/tools-youtube-migrator) | ![No badge](https://gitlab.vieon.vn/ott/legacy/tools-youtube-migrator/badges/master/coverage.svg) |
| [legacy / ott / tools-facebook-migrator](https://gitlab.vieon.vn/ott/legacy/tools-facebook-migrator) | ![No badge](https://gitlab.vieon.vn/ott/legacy/tools-facebook-migrator/badges/master/coverage.svg) |
| [legacy / ott / tools-admin](https://gitlab.vieon.vn/ott/legacy/tools-admin) | ![No badge](https://gitlab.vieon.vn/ott/legacy/tools-admin/badges/master/coverage.svg) |
| [legacy / ott / services-vieplay-tracking-logs](https://gitlab.vieon.vn/ott/legacy/services-vieplay-tracking-logs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-vieplay-tracking-logs/badges/master/coverage.svg) |
| [legacy / ott / services-vieplay-tracking-jobs](https://gitlab.vieon.vn/ott/legacy/services-vieplay-tracking-jobs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-vieplay-tracking-jobs/badges/master/coverage.svg) |
| [legacy / ott / services-versioning](https://gitlab.vieon.vn/ott/legacy/services-versioning) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-versioning/badges/master/coverage.svg) |
| [legacy / ott / services-user-behavior](https://gitlab.vieon.vn/ott/legacy/services-user-behavior) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-user-behavior/badges/master/coverage.svg) |
| [legacy / ott / transcode](https://gitlab.vieon.vn/ott/legacy/transcode) | ![No badge](https://gitlab.vieon.vn/ott/legacy/transcode/badges/master/coverage.svg) |
| [legacy / ott / services-setting](https://gitlab.vieon.vn/ott/legacy/services-setting) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-setting/badges/master/coverage.svg) |
| [legacy / ott / services-searching](https://gitlab.vieon.vn/ott/legacy/services-searching) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-searching/badges/master/coverage.svg) |
| [legacy / ott / services-pushing](https://gitlab.vieon.vn/ott/legacy/services-pushing) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-pushing/badges/master/coverage.svg) |
| [legacy / ott / services-metadata](https://gitlab.vieon.vn/ott/legacy/services-metadata) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-metadata/badges/master/coverage.svg) |
| [legacy / ott / services-loadtest](https://gitlab.vieon.vn/ott/legacy/services-loadtest) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-loadtest/badges/master/coverage.svg) |
| [legacy / ott / services-live-feed](https://gitlab.vieon.vn/ott/legacy/services-live-feed) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-live-feed/badges/master/coverage.svg) |
| [legacy / ott / services-image-processing](https://gitlab.vieon.vn/ott/legacy/services-image-processing) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-image-processing/badges/master/coverage.svg) |
| [legacy / ott / services-file-manager](https://gitlab.vieon.vn/ott/legacy/services-file-manager) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-file-manager/badges/master/coverage.svg) |
| [legacy / ott / services-dzones-logs](https://gitlab.vieon.vn/ott/legacy/services-dzones-logs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-dzones-logs/badges/master/coverage.svg) |
| [legacy / ott / services-direct-message](https://gitlab.vieon.vn/ott/legacy/services-direct-message) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-direct-message/badges/master/coverage.svg) |
| [legacy / ott / services-common](https://gitlab.vieon.vn/ott/legacy/services-common) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-common/badges/master/coverage.svg) |
| [legacy / ott / services-caching](https://gitlab.vieon.vn/ott/legacy/services-caching) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-caching/badges/master/coverage.svg) |
| [legacy / ott / services-billing](https://gitlab.vieon.vn/ott/legacy/services-billing) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-billing/badges/master/coverage.svg) |
| [legacy / ott / services-analytic-proxy](https://gitlab.vieon.vn/ott/legacy/services-analytic-proxy) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-analytic-proxy/badges/master/coverage.svg) |
| [legacy / ott / services-analytic-jobs](https://gitlab.vieon.vn/ott/legacy/services-analytic-jobs) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-analytic-jobs/badges/master/coverage.svg) |
| [legacy / ott / services-ads-integration](https://gitlab.vieon.vn/ott/legacy/services-ads-integration) | ![No badge](https://gitlab.vieon.vn/ott/legacy/services-ads-integration/badges/master/coverage.svg) |
| [legacy / ott / did-rn-voting](https://gitlab.vieon.vn/ott/legacy/did-rn-voting) | ![No badge](https://gitlab.vieon.vn/ott/legacy/did-rn-voting/badges/master/coverage.svg) |
| [legacy / ott / devices_tool_user_engagement](https://gitlab.vieon.vn/ott/legacy/devices_tool_user_engagement) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices_tool_user_engagement/badges/master/coverage.svg) |
| [legacy / ott / devices-web-social-hub](https://gitlab.vieon.vn/ott/legacy/devices-web-social-hub) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-web-social-hub/badges/master/coverage.svg) |
| [legacy / ott / devices-web-mobile](https://gitlab.vieon.vn/ott/legacy/devices-web-mobile) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-web-mobile/badges/master/coverage.svg) |
| [legacy / ott / devices-web-content-hub](https://gitlab.vieon.vn/ott/legacy/devices-web-content-hub) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-web-content-hub/badges/master/coverage.svg) |
| [legacy / ott / devices-web-admin-hub](https://gitlab.vieon.vn/ott/legacy/devices-web-admin-hub) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-web-admin-hub/badges/master/coverage.svg) |
| [legacy / ott / devices-web](https://gitlab.vieon.vn/ott/legacy/devices-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-web/badges/master/coverage.svg) |
| [legacy / ott / devices-vis-web](https://gitlab.vieon.vn/ott/legacy/devices-vis-web) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-vis-web/badges/master/coverage.svg) |
| [legacy / ott / devices-smart-tv-ss](https://gitlab.vieon.vn/ott/legacy/devices-smart-tv-ss) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-smart-tv-ss/badges/master/coverage.svg) |
| [legacy / ott / devices-smart-tv](https://gitlab.vieon.vn/ott/legacy/devices-smart-tv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-smart-tv/badges/master/coverage.svg) |
| [legacy / ott / devices-services-social-hub](https://gitlab.vieon.vn/ott/legacy/devices-services-social-hub) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-services-social-hub/badges/master/coverage.svg) |
| [legacy / ott / devices-publisher-sites](https://gitlab.vieon.vn/ott/legacy/devices-publisher-sites) | ![No badge](https://gitlab.vieon.vn/ott/legacy/devices-publisher-sites/badges/master/coverage.svg) |
| [legacy / ott / deamons-tracking-recommendation](https://gitlab.vieon.vn/ott/legacy/deamons-tracking-recommendation) | ![No badge](https://gitlab.vieon.vn/ott/legacy/deamons-tracking-recommendation/badges/master/coverage.svg) |
| [legacy / ott / dcine](https://gitlab.vieon.vn/ott/legacy/dcine) | ![No badge](https://gitlab.vieon.vn/ott/legacy/dcine/badges/master/coverage.svg) |
| [legacy / ott / daemons-vieplay-insight-crawler](https://gitlab.vieon.vn/ott/legacy/daemons-vieplay-insight-crawler) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-vieplay-insight-crawler/badges/master/coverage.svg) |
| [legacy / ott / daemons-user-engagement](https://gitlab.vieon.vn/ott/legacy/daemons-user-engagement) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-user-engagement/badges/master/coverage.svg) |
| [legacy / ott / daemons-user-behaviours](https://gitlab.vieon.vn/ott/legacy/daemons-user-behaviours) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-user-behaviours/badges/master/coverage.svg) |
| [legacy / ott / daemons-upload](https://gitlab.vieon.vn/ott/legacy/daemons-upload) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-upload/badges/master/coverage.svg) |
| [legacy / ott / daemons-update-engaged-time](https://gitlab.vieon.vn/ott/legacy/daemons-update-engaged-time) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-update-engaged-time/badges/master/coverage.svg) |
| [legacy / ott / daemons-transcode](https://gitlab.vieon.vn/ott/legacy/daemons-transcode) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-transcode/badges/master/coverage.svg) |
| [legacy / ott / daemons-subscriptions](https://gitlab.vieon.vn/ott/legacy/daemons-subscriptions) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-subscriptions/badges/master/coverage.svg) |
| [legacy / ott / daemons-remove-temp-content](https://gitlab.vieon.vn/ott/legacy/daemons-remove-temp-content) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-remove-temp-content/badges/master/coverage.svg) |
| [legacy / ott / daemons-recommendations-data](https://gitlab.vieon.vn/ott/legacy/daemons-recommendations-data) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-recommendations-data/badges/master/coverage.svg) |
| [legacy / ott / daemons-ott-caching-queues-processing](https://gitlab.vieon.vn/ott/legacy/daemons-ott-caching-queues-processing) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-ott-caching-queues-processing/badges/master/coverage.svg) |
| [legacy / ott / daemons-live-feed](https://gitlab.vieon.vn/ott/legacy/daemons-live-feed) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-live-feed/badges/master/coverage.svg) |
| [legacy / ott / daemons-like](https://gitlab.vieon.vn/ott/legacy/daemons-like) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-like/badges/master/coverage.svg) |
| [legacy / ott / daemons-content](https://gitlab.vieon.vn/ott/legacy/daemons-content) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-content/badges/master/coverage.svg) |
| [legacy / ott / daemons-caching](https://gitlab.vieon.vn/ott/legacy/daemons-caching) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-caching/badges/master/coverage.svg) |
| [legacy / ott / daemons-ads-ants](https://gitlab.vieon.vn/ott/legacy/daemons-ads-ants) | ![No badge](https://gitlab.vieon.vn/ott/legacy/daemons-ads-ants/badges/master/coverage.svg) |
| [legacy / ott / _documents_](https://gitlab.vieon.vn/ott/legacy/_documents_) | ![No badge](https://gitlab.vieon.vn/ott/legacy/_documents_/badges/master/coverage.svg) |
| [vieon-platforms / devices-html](https://gitlab.vieon.vn/vieon-platforms/devices-html) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/devices-html/badges/master/coverage.svg) |
| [vieon-services / images](https://gitlab.vieon.vn/vieon-services/images) | ![No badge](https://gitlab.vieon.vn/vieon-services/images/badges/master/coverage.svg) |
| [vieon-services / socket](https://gitlab.vieon.vn/vieon-services/socket) | ![No badge](https://gitlab.vieon.vn/vieon-services/socket/badges/master/coverage.svg) |
| [legacy / ott / content-manager](https://gitlab.vieon.vn/ott/legacy/content-manager) | ![No badge](https://gitlab.vieon.vn/ott/legacy/content-manager/badges/master/coverage.svg) |
| [legacy / Artificial Intelligent / ott-devices-html](https://gitlab.vieon.vn/ott/artificial-intelligent/ott-devices-html) | ![No badge](https://gitlab.vieon.vn/ott/artificial-intelligent/ott-devices-html/badges/master/coverage.svg) |
| [Quality Assurance / ott-services / Automation](https://gitlab.vieon.vn/quality-assurance/ott-services/automation) | ![No badge](https://gitlab.vieon.vn/quality-assurance/ott-services/automation/badges/master/coverage.svg) |
| [Quality Assurance / ott-services / api-testing](https://gitlab.vieon.vn/quality-assurance/ott-services/api-testing) | ![No badge](https://gitlab.vieon.vn/quality-assurance/ott-services/api-testing/badges/master/coverage.svg) |
| [vieon-services / giftcode](https://gitlab.vieon.vn/vieon-services/giftcode) | ![No badge](https://gitlab.vieon.vn/vieon-services/giftcode/badges/master/coverage.svg) |
| [Thang Nguyen / app-ios](https://gitlab.vieon.vn/thang.nguyen.minh/app-ios) | ![No badge](https://gitlab.vieon.vn/thang.nguyen.minh/app-ios/badges/master/coverage.svg) |
| [vieon-services / content-assurance](https://gitlab.vieon.vn/vieon-services/content-assurance) | ![No badge](https://gitlab.vieon.vn/vieon-services/content-assurance/badges/master/coverage.svg) |
| [legacy / ott / HTML TV](https://gitlab.vieon.vn/ott/legacy/html-tv) | ![No badge](https://gitlab.vieon.vn/ott/legacy/html-tv/badges/master/coverage.svg) |
| [vieon-services / cm-v5](https://gitlab.vieon.vn/vieon-services/cm-v5) | ![No badge](https://gitlab.vieon.vn/vieon-services/cm-v5/badges/master/coverage.svg) |
| [data / Retrieve Kafka Sample Logs](https://gitlab.vieon.vn/data/retrieve-kafka-sample-logs) | ![No badge](https://gitlab.vieon.vn/data/retrieve-kafka-sample-logs/badges/master/coverage.svg) |
| [platform / livetv](https://gitlab.vieon.vn/platform/livetv) | ![No badge](https://gitlab.vieon.vn/platform/livetv/badges/master/coverage.svg) |
| [data / ott-data-etl-active-user](https://gitlab.vieon.vn/data/ott-data-etl-active-user) | ![No badge](https://gitlab.vieon.vn/data/ott-data-etl-active-user/badges/master/coverage.svg) |
| [data / data_project_template](https://gitlab.vieon.vn/data/data_project_template) | ![No badge](https://gitlab.vieon.vn/data/data_project_template/badges/master/coverage.svg) |
| [data / deamon_go_ccu_vip](https://gitlab.vieon.vn/data/deamon_go_ccu_vip) | ![No badge](https://gitlab.vieon.vn/data/deamon_go_ccu_vip/badges/master/coverage.svg) |
| [platform / cdn / cdn-config](https://gitlab.vieon.vn/platform/cdn/cdn-config) | ![No badge](https://gitlab.vieon.vn/platform/cdn/cdn-config/badges/master/coverage.svg) |
| [platform / devops](https://gitlab.vieon.vn/platform/devops) | ![No badge](https://gitlab.vieon.vn/platform/devops/badges/master/coverage.svg) |
| [vieon-platforms / Android](https://gitlab.vieon.vn/vieon-platforms/android) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/android/badges/master/coverage.svg) |
| [vieon-platforms / iOS](https://gitlab.vieon.vn/vieon-platforms/ios) | ![No badge](https://gitlab.vieon.vn/vieon-platforms/ios/badges/master/coverage.svg) |
| [vieon-services / playlist](https://gitlab.vieon.vn/vieon-services/playlist) | ![No badge](https://gitlab.vieon.vn/vieon-services/playlist/badges/master/coverage.svg) |
