package main

import (
	"bai_tap/pkg/list_number"
	"bai_tap/pkg/readfile"
	"bai_tap/pkg/writefile"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	path := "D:\\git\\go\\go\\bai_tap\\input.txt"
	lines, err := readfile.ReadLines(path)

	if err != nil {
		fmt.Printf("readLines: %s", err)
	}

	var Arr []int
	for i := 0; i < len(lines); i++ {
		iArr, err := readfile.Slice_Atoi(strings.Split(lines[i], " "))
		if err != nil {
			log.Print("str failed: ", err)
			return
		}
		for j := 0; j < len(iArr); j++ {
			Arr = append(Arr, iArr[j])
		}
	}

	n := Arr[0]
	out_put1 := "D:\\git\\go\\go\\bai_tap\\output1.txt"
	List_even_number := list_number.List_even_number_4(Arr)
	Sum_odd := list_number.Sum_odd_4(Arr)
	Search_n := list_number.Search_b_4(Arr, n)
	Search_songuyento := list_number.Search_Songuyento_4(Arr)

	strn := "\n"
	str_find_even := "find_even: "
	str_sum_odd := "sum_odd: "
	str_find_n := "n_appear_array_a: "
	str_find_prime := "prime_number_appear_array_a: "
	//delete file output
	e := os.Remove(out_put1)
	if e != nil {
		fmt.Printf("remove false: ", e)
	}
	//find even
	if err := writefile.Writestr(str_find_even, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.WriteLines(List_even_number, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	// sum odd
	if err := writefile.Writestr(strn, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.Writestr(str_sum_odd, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.Writeint(Sum_odd, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	//find n in array a
	if err := writefile.Writestr(strn, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.Writestr(str_find_n, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.Writeint(Search_n, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	// find prime number aray a
	if err := writefile.Writestr(strn, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.Writestr(str_find_prime, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
	if err := writefile.WriteLines(Search_songuyento, out_put1); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
}
