package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/template"

	u "github.com/sunshine69/golang-tools/utils"
	"github.com/xanzy/go-gitlab"
)

type Config struct {
	Host  string `json:"host"`
	Token string `json:"token"`
}

type Converage struct {
	Name   string
	Weburl string
}

func Writestr(str string, path string) error {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()
	w := bufio.NewWriter(file)
	w.WriteString(str)
	return w.Flush()
}

func main() {
	File_converage := "D:\\git\\go\\go\\bai_tap\\coverage.md"
	file, e := ioutil.ReadFile("../config.json")
	Str_converage := "| Project name | Coverrage |\n| -- | -- |\n"
	Str_template := template.New("Template_converage")

	if e != nil {
		fmt.Printf("Config file error: %v\n", e)
		os.Exit(1)
	}

	var config Config
	json.Unmarshal(file, &config)
	//fmt.Printf("Results: %+v\n", config)

	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Host))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	opt := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	for {
		projectService := git.Projects
		projects, resp, err := projectService.ListProjects(opt)
		u.CheckErr(err, "Projects.ListProjects")

		for _, row := range projects {
			Str := Converage{row.NameWithNamespace, row.WebURL}
			Str_template, _ = Str_template.Parse("| [{{.Name}}]({{.Weburl}}) | ![No badge]({{.Weburl}}/badges/master/coverage.svg) |\n")
			builder := &strings.Builder{}
			if err := Str_template.Execute(builder, Str); err != nil {
				panic(err)
			}
			s := builder.String()
			Str_converage += s
		}

		// Exit the loop when we've seen all pages.
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}
	if err := Writestr(Str_converage, File_converage); err != nil {
		fmt.Printf("writeLines: %s", err)
	}
}
