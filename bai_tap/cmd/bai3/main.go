package main

import (
	"bai_tap/pkg/integer"
	"bai_tap/pkg/list_number"
	"fmt"
)

func main() {
	n := 5
	k := 10
	year := 2000
	Startyear := 2010
	EndYear := 2021
	// list array
	slice := []int{21, 20, 12, 21, 29, 30, 23, 21, 8, 76, 98, 9, 21, 90}
	b := 21

	S1 := integer.Sumint1(n)
	S2 := integer.Sumint2(n)
	S3 := integer.Sumint3(n)
	S4 := integer.Sumint4(n)
	S5 := integer.Sumint5(n)
	Fibo6 := integer.Fibo(n)

	Search_sochinhphuong := integer.Search_SochinhPhuong(n)
	Search_songuyento := integer.Search_Songuyento(n)
	Search_namnhuan := integer.Search_Namnhuan(year)
	Search_Multinamnhuan := integer.Search_MultiNamnhuan(Startyear, EndYear)

	List_even_number := list_number.List_even_number(slice)
	Sum_odd := list_number.Sum_odd(slice)
	Search_b := list_number.Search_b(slice, b)
	Search_songuyento_array := list_number.Search_Songuyento(slice)

	fmt.Printf("S(n) = 1 + 2 + 3 + .... + n = %d \n", S1)
	fmt.Printf("S(n)=1+3+5+...+(2n+1) = %d \n", S2)
	fmt.Printf("S(n)=1+2^2+3^3+...+n^n = %d \n", S3)
	fmt.Printf("S(n)=1+1/2+1/3+...+1/n = %f \n", S4)
	fmt.Printf("S(n)=1*2*4*...*2n = %d \n", S5)
	fmt.Printf("fibo(n)=1+2+3+5+8+...+f(n−1)= %d \n", Fibo6)

	if S1 < k {
		fmt.Printf("S(n) = 1 + 2 + 3 + .... + n = %d \n", S1)
	} else {
		fmt.Printf("k=%d, S(n)=%d, S(n) Phai nho hon k \n", k, S1)
	}

	if Search_sochinhphuong == 1 {
		fmt.Printf("%d la so chinh phuong \n", n)
	} else {
		fmt.Printf("%d Khong la so chinh phuong \n", n)
	}

	if Search_songuyento == true {
		fmt.Printf("%d la so nguyen to \n", n)
	} else {
		fmt.Printf("%d khong la so nguyen to \n", n)
	}

	if Search_namnhuan == 0 {
		fmt.Printf("Nam %d la Nam Nhuan \n", year)
	} else {
		fmt.Printf("Nam %d khong la Nam Nhuan \n", year)
	}

	fmt.Printf("Cac nam nhuan tu nam %d den nam %d la: %d \n", Startyear, EndYear, Search_Multinamnhuan)

	fmt.Printf("Cac phan tu mang a: %d \n", slice)
	fmt.Printf("Cac gia tri chan trong mang a la: %d \n", List_even_number)
	fmt.Printf("Tong cac so le trong mang a la: %d \n", Sum_odd)
	fmt.Printf("so lan xuat hien cua b: %d trong mang a la: %d \n", b, Search_b)

	fmt.Printf("Danh sach so nguyen to trong mang a la: %d", Search_songuyento_array)
}
