package main

import (
	"fmt"
)

func SochinhPhuong(n int) int {
	for i := 1; i*i <= n; i++ {
		if i*i == n {
			return 1
		}
	}
	return 0
}

func main() {
	n := 25
	check := SochinhPhuong(n)
	if check == 1 {
		fmt.Printf("%d la so chinh phuong", n)
	} else {
		fmt.Printf("%d Khong la so chinh phuong", n)
	}
}
