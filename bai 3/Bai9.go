package main

import (
	"fmt"
)

func Songuyento(n int) int {
	a := n / 2
	count := 0
	for i := 2; i <= a; i++ {
		if n%i == 0 {
			count++
		}
	}
	return count
}

func main() {
	n := 5
	result := Songuyento(n)
	if result == 0 {
		fmt.Printf("%d la so nguyen to", n)
	} else {
		fmt.Printf("%d khong la so nguyen to", n)
	}
}
