package main

import "fmt"

func NhapN() int {
	var n int
	fmt.Println("Nhap vao So nguyen duong n: ")
	fmt.Scan(&n)
	if n <= 0 {
		fmt.Println("Nhap sai vui long nhap lai: ")
		NhapN()
	}
	return n
}

func Tinhtong1(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		result += i
	}
	return result
}

func Tinhtong2(n int) int {
	result := 0
	for i := 0; i <= n; i++ {
		result += (2*i + 1)
	}
	return result
}

func Tinhtong3(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		q := 1
		for j := 1; j <= i; j++ {
			q *= i
		}
		result += q
	}
	return result
}

func Tinhtong4(n int) float64 {
	result := 0.0
	for i := 1; i <= n; i++ {
		result += 1.0 / float64(i)
	}
	return result
}

func Tinhtong5(n int) int {
	result := 1
	for i := 1; i <= n; i++ {
		result *= 2 * i
	}
	return result
}

func main() {
	n := NhapN()
	Tong1 := Tinhtong1(n)
	Tong2 := Tinhtong2(n)
	Tong3 := Tinhtong3(n)
	Tong4 := Tinhtong4(n)
	Tong5 := Tinhtong5(n)
	fmt.Printf("S(n) = 1 + 2 + 3 + .... + n = %d \n", Tong1)
	fmt.Printf("S(n)=1+3+5+...+(2n+1) = %d \n", Tong2)
	fmt.Printf("S(n)=1+2^2+3^3+...+n^n = %d \n", Tong3)
	fmt.Printf("S(n)=1+1/2+1/3+...+1/n = %f \n", Tong4)
	fmt.Printf("S(n)=1*2*4*...*2n = %d \n", Tong5)
}
