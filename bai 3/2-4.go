package main

import "fmt"

func Nhap() int {
	var n int
	fmt.Println("Nhap vao So phan tu cua mang: ")
	fmt.Scan(&n)
	if n <= 0 {
		fmt.Println("Nhap sai vui long nhap lai: ")
		Nhap()
	}
	return n
}

func TimSonguyento(n int) bool {
	b := n / 2
	if n < 2 {
		return false
	}
	for i := 2; i <= b; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func Songuyento(a []int, n int) {
	for i := 0; i < n; i++ {
		if TimSonguyento(a[i]) == true {
			fmt.Printf("%d \t", a[i])
		}
	}
}

func main() {
	n := Nhap()
	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Printf("\nNhap phan tu a[%d] ", i)
		fmt.Scan(&a[i])
	}
	fmt.Printf("Cac phan tu mang a: %d \n", a)

	fmt.Println("\n Danh sach so nguyen to trong mang a:")
	Songuyento(a, n)
}
