package main

import (
	"fmt"
)

func Namnhuan(year int) int {
	if year%400 == 0 {
		return 0
	}
	if year%4 == 0 && year%100 != 0 {
		return 0
	}
	return 1
}

func main() {
	year := 2001
	result := Namnhuan(year)
	if result == 0 {
		fmt.Printf("%d la Nam Nhuan", year)
	} else {
		fmt.Printf("%d khong la Nam Nhuan", year)
	}
}
