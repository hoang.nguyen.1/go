package main

import "fmt"

func Nhap() int {
	var n int
	fmt.Println("Nhap vao So phan tu cua mang: ")
	fmt.Scan(&n)
	if n <= 0 {
		fmt.Println("Nhap sai vui long nhap lai: ")
		Nhap()
	}
	return n
}

func Nhapb() int {
	var b int
	fmt.Println("Nhap vao So nguyen duong b: ")
	fmt.Scan(&b)
	if b <= 0 {
		fmt.Println("Nhap sai vui long nhap lai: ")
		Nhapb()
	}
	return b
}

func Sochan(a []int) []int {
	var result []int
	for i := 0; i < len(a); i++ {
		if a[i]%2 == 0 {
			result = append(result, a[i])
		}
	}
	return result
}

func Tongle(a []int) int {
	result := 0
	for i := 0; i < len(a); i++ {
		if a[i]%2 != 0 {
			result += a[i]
		}
	}
	return result
}

func Timb(a []int, b int) int {
	count := 0
	for i := 0; i < len(a); i++ {
		if a[i] == b {
			count++
		}
	}
	return count
}

func main() {
	n := Nhap()
	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Printf("\nNhap phan tu a[%d] ", i)
		fmt.Scan(&a[i])
	}
	fmt.Printf("Cac phan tu mang a: %d \n", a)
	Sochan := Sochan(a)
	fmt.Printf("Cac gia tri chan trong mang a la: %d \n", Sochan)
	Tongle := Tongle(a)
	fmt.Printf("Tong cac so le trong mang a la: %d \n", Tongle)
	b := Nhapb()
	count := Timb(a, b)
	fmt.Printf("so lan xuat hien cua b trong mang a la: %d \n", count)
}
