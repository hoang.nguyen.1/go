package main

import (
	"fmt"
)

func Namnhuan(Startyear, EndYear int) []int {
	var a []int
	for i := Startyear; i <= EndYear; i++ {
		if i%400 == 0 {
			a = append(a, i)
		}
		if i%4 == 0 && i%100 != 0 {
			a = append(a, i)
		}
	}
	return a
}

func main() {
	Startyear := 2010
	EndYear := 2021
	result := Namnhuan(Startyear, EndYear)
	fmt.Printf("Cac nam nhuan la: %d \n", result)
}
