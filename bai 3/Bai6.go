package main

import (
	"fmt"
)

func Fibo(f int) int {
	Fib := []int{1, 1}
	for i := 3; i <= f; i++ {
		nextFib := Fib[0] + Fib[1]
		Fib = []int{Fib[1], nextFib}
	}
	if f >= 1 {
		return Fib[1]
	}
	return Fib[0]
}

func SumFibo(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		F := Fibo(i)
		result += F
		fmt.Println(F)
	}
	return result
}

func main() {
	n := 3
	S := SumFibo(n)
	fmt.Printf("fibo(n)=1+2+3+5+8+...+f(n−1)= %d", S)
}
